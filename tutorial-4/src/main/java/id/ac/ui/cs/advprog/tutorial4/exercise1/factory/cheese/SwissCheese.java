package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class SwissCheese implements Cheese {

    public String toString() {
        return "Spreaded Swiss Cheese";
    }
}
