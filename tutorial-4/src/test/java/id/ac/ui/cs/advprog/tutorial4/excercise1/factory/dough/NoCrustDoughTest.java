package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class NoCrustDoughTest {
    private NoCrustDough dough;

    @Before
    public void setUp() {
        dough = new NoCrustDough();
    }

    @Test
    public void testString() {
        assertEquals(dough.toString(), "NoCrust style no crust to bite");
    }
}
