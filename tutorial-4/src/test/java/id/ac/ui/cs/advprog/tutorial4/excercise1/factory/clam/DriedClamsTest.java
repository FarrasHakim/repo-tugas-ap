package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DriedClamsTest {
    private DriedClams clam;

    @Before
    public void setUp() {
        clam = new DriedClams();
    }

    @Test
    public void testString() {
        assertEquals(clam.toString(), "Dried Clams from Depok");
    }
}
