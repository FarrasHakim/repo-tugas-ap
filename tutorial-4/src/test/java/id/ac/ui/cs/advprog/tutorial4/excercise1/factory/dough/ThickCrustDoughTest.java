package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {
    private ThickCrustDough dough;

    @Before
    public void setUp() {
        dough = new ThickCrustDough();
    }

    @Test
    public void testString() {
        assertEquals(dough.toString(), "ThickCrust style extra thick crust dough");
    }
}
