package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ParmesanCheeseTest {
    private ParmesanCheese parmesan;

    @Before
    public void setUp() {
        parmesan = new ParmesanCheese();
    }

    @Test
    public void testString() {
        assertEquals(parmesan.toString(), "Shredded Parmesan");
    }
}
