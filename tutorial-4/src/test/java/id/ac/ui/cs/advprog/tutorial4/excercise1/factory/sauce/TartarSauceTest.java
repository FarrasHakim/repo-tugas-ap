package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TartarSauceTest {
    private TartarSauce sauce;

    @Before
    public void setUp() {
        sauce = new TartarSauce();
    }

    @Test
    public void testString() {
        assertEquals(sauce.toString(), "Tasty tartar sauce");
    }
}
