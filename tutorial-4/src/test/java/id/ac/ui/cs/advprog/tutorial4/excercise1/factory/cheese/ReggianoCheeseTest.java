package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {
    private ReggianoCheese reggiano;

    @Before
    public void setUp() {
        reggiano = new ReggianoCheese();
    }

    @Test
    public void testString() {
        assertEquals(reggiano.toString(), "Reggiano Cheese");
    }
}
