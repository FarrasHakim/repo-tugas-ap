package id.ac.ui.cs.advprog.tutorial4.excercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DepokPizzaIngredientFactoryTest {
    private DepokPizzaIngredientFactory factory;
    private Dough dough;
    private Sauce sauce;
    private Cheese cheese;
    private Veggies[] veggies;
    private Clams clam;

    @Before
    public void setUp() {
        factory = new DepokPizzaIngredientFactory();
        dough = factory.createDough();
        sauce = factory.createSauce();
        cheese = factory.createCheese();
        veggies = factory.createVeggies();
        clam = factory.createClam();
    }

    @Test
    public void testClassName() {
        assertEquals(factory.getClass().getSimpleName(), "DepokPizzaIngredientFactory");
    }

    @Test
    public void testDough() {
        assertEquals(dough.getClass().getSimpleName(), "NoCrustDough");
    }

    @Test
    public void testSauce() {
        assertEquals(sauce.getClass().getSimpleName(), "TartarSauce");
    }

    @Test
    public void testCheese() {
        assertEquals(cheese.getClass().getSimpleName(), "SwissCheese");
    }

    @Test
    public void testVeggie() {
        assertEquals(veggies[0].getClass().getSimpleName(), "Garlic");
        assertEquals(veggies[1].getClass().getSimpleName(), "Onion");
        assertEquals(veggies[2].getClass().getSimpleName(), "Mushroom");
        assertEquals(veggies[3].getClass().getSimpleName(), "Cucumber");
    }

    @Test
    public void testClam() {
        assertEquals(clam.getClass().getSimpleName(), "DriedClams");
    }
}
