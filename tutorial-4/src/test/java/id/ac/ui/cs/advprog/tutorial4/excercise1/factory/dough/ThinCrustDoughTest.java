package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinCrustDoughTest {
    private ThinCrustDough dough;

    @Before
    public void setUp() {
        dough = new ThinCrustDough();
    }

    @Test
    public void testString() {
        assertEquals(dough.toString(), "Thin Crust Dough");
    }
}
