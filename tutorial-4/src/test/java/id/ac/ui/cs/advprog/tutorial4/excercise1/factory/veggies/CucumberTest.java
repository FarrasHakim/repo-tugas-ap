package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CucumberTest {
    private Cucumber veggie;

    @Before
    public void setUp() {
        veggie = new Cucumber();
    }

    @Test
    public void testString() {
        assertEquals(veggie.toString(), "Cucumber");
    }
}
