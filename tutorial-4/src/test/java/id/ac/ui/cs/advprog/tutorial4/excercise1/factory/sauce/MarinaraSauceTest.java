package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {
    private MarinaraSauce sauce;

    @Before
    public void setUp() {
        sauce = new MarinaraSauce();
    }

    @Test
    public void testString() {
        assertEquals(sauce.toString(), "Marinara Sauce");
    }
}
