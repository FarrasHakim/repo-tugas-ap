package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SpinachTest {
    private Spinach veggie;

    @Before
    public void setUp() {
        veggie = new Spinach();
    }

    @Test
    public void testString() {
        assertEquals(veggie.toString(), "Spinach");
    }
}
