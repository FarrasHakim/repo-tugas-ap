package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class OnionTest {
    private Onion veggie;

    @Before
    public void setUp() {
        veggie = new Onion();
    }

    @Test
    public void testString() {
        assertEquals(veggie.toString(), "Onion");
    }
}
