package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {
    private FrozenClams clam;

    @Before
    public void setUp() {
        clam = new FrozenClams();
    }

    @Test
    public void testString() {
        assertEquals(clam.toString(), "Frozen Clams from Chesapeake Bay");
    }
}
