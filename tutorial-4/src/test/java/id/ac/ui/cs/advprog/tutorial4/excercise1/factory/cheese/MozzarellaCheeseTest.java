package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private MozzarellaCheese mozzarella;

    @Before
    public void setUp() {
        mozzarella = new MozzarellaCheese();
    }

    @Test
    public void testString() {
        assertEquals(mozzarella.toString(), "Shredded Mozzarella");
    }
}
