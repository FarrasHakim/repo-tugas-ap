package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FreshClamsTest {
    private FreshClams clam;

    @Before
    public void setUp() {
        clam = new FreshClams();
    }

    @Test
    public void testString() {
        assertEquals(clam.toString(), "Fresh Clams from Long Island Sound");
    }
}
