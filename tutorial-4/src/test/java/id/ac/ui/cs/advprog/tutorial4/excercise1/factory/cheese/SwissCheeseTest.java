package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SwissCheeseTest {
    private SwissCheese swiss;

    @Before
    public void setUp() {
        swiss = new SwissCheese();
    }

    @Test
    public void testString() {
        assertEquals(swiss.toString(), "Spreaded Swiss Cheese");
    }
}
