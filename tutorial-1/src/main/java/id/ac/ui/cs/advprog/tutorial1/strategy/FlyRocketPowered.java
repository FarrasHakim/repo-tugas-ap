package id.ac.ui.cs.advprog.tutorial1.strategy;

public class FlyRocketPowered implements FlyBehavior {

    @Override
    public void fly() {
        System.out.println("I use rockets to fly. Whooosh!");
    }
}
