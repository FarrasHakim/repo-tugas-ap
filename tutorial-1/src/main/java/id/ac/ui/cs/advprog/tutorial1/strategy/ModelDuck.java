package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {

    public ModelDuck() {
        FlyNoWay flyBvr = new FlyNoWay();
        Squeak quackBvr = new Squeak();

        this.setFlyBehavior(flyBvr);
        this.setQuackBehavior(quackBvr);
    }

    @Override
    public void display() {
        System.out.println("I'm a decoy");
    }
}
