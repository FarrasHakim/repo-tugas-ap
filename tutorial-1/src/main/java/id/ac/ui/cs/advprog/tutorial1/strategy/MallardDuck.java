package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    public MallardDuck() {
        FlyWithWings flyBvr = new FlyWithWings();
        Quack quackBvr = new Quack();

        this.setFlyBehavior(flyBvr);
        this.setQuackBehavior(quackBvr);
    }

    @Override
    public void display() {
        System.out.println("I look like a Mallard");
    }
}
