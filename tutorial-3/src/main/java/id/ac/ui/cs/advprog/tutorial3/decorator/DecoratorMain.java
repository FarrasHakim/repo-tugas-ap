package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.NoCrustSandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThickBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.ThinBunBurger;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cucumber;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;

public class DecoratorMain {
    public static void main(String[] args) {
        Food beefBurger = new ThickBunBurger();
        beefBurger = new BeefMeat(beefBurger);
        beefBurger = new Lettuce(beefBurger);
        beefBurger = new Cheese(beefBurger);
        beefBurger = new Tomato(beefBurger);
        beefBurger = new TomatoSauce(beefBurger);

        System.out.println(beefBurger.getDescription());
        System.out.println(beefBurger.cost());
    }
}
