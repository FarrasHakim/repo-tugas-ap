package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompositeMain {
    public static void main(String[] args) {
        Company pt = new Company();

        Employees ceo = new Ceo("Janet", 210000.00);
        Employees cto = new Cto("Jacob", 200000.00);
        Employees frontEnd = new FrontendProgrammer("Bobby", 90000.00);
        Employees security = new SecurityExpert("Johnny", 100000.00);

        pt.addEmployee(ceo);
        pt.addEmployee(cto);
        pt.addEmployee(frontEnd);
        pt.addEmployee(security);

        System.out.println(ceo.getName());
        System.out.println(security.getSalary());
        System.out.println(frontEnd.getRole());

    }
}
